import 'dart:io';
import 'package:fina_sqflite/models/orderListModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqlite_api.dart';


class DbService {

  DbService._();

  static final DbService db = DbService._();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

   initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TheLoan.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Loan ("
          "id INTEGER PRIMARY KEY,"
          "xName TEXT,"
          "xId TEXT,"
          "xTime TEXT"
          ")");
    });
  }

  Future insertProgram(orderListModel newProgram) async {
    final db = await database;

    await db.insert("Loan", newProgram.toJson());
  }

  Future<List<orderListModel>> loadPrograms() async {
    final db = await database;

    var res = await db.query("Loan");
    List<orderListModel> list = res.isNotEmpty ? res.map((Program) => orderListModel.fromJson(Program)).toList()
        : [];
    return list;
  }


  deleteProgram(String id) async {
    final db = await database;

    db.delete("Loan", where: "xId = ?", whereArgs: [id]);
  }


}
