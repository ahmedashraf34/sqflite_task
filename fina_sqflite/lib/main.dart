import 'package:fina_sqflite/services/DbService.dart';
import 'package:fina_sqflite/views/FavouritProgramsView.dart';
import 'package:fina_sqflite/views/ProgramsView.dart';
import 'package:flutter/material.dart';

main() async {
  return runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'sqFlite',
      debugShowCheckedModeBanner: false,
      home: HomeView(),
    );
  }
}

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  var db = DbService.db;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text(
                "My Programs",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
                textScaleFactor: 2,
              ),
              onPressed: () {
                db.loadPrograms().then((v){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) {
                      return ProgramsView(v);
                    }),
                  );
                });

              },
              color: Colors.blue,
              padding: EdgeInsets.all(10),
            ),
            RaisedButton(
              child: Text(
                "Favourits Programs",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
                textScaleFactor: 2,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (BuildContext context) {
                    return FavouritProgramsView();
                  }),
                );
              },
              color: Colors.blue,
              padding: EdgeInsets.all(10),
            ),
          ],
        ),
      ),
    );
  }
}
