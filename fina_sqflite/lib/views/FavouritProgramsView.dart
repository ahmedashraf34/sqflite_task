import 'package:fina_sqflite/models/orderListModel.dart';
import 'package:fina_sqflite/services/DbService.dart';
import 'package:flutter/material.dart';

class FavouritProgramsView extends StatefulWidget {
  @override
  _FavouritProgramsViewState createState() => _FavouritProgramsViewState();
}

class _FavouritProgramsViewState extends State<FavouritProgramsView> {
  var db = DbService.db;
  List<orderListModel> favouritsList = [];

  var isLoadingFavourits = false;

  getProgramsList() async {
    setState(() {
      isLoadingFavourits = true;
    });
    db.loadPrograms().then((list) {
      favouritsList = list;
      setState(() {
        isLoadingFavourits = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getProgramsList();
  }

  removeOrderList(int index,String id){
    db.deleteProgram(id);
    favouritsList.removeAt(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favorite programs'),
      ),
      body: isLoadingFavourits
          ? Center(
              child: CircularProgressIndicator(),
            )
          : favouritsList.length == 0
              ? Center(child: Text("لاتوجد"))
              : ListView.builder(
                  itemCount: favouritsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text('${favouritsList[index].xName}'),
                      trailing: GestureDetector(
                        onTap: () {
                          setState(() {
                            removeOrderList(index,favouritsList[index].xId);
                          });
                        },
                        child: Icon(
                          Icons.favorite,
                          color: Colors.red,
                        ),
                      ),
                      // selected: programList[index].selected,
                    );
                  },
                ),
    );
  }
}
