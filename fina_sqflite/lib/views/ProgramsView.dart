import 'package:fina_sqflite/models/orderListModel.dart';
import 'package:fina_sqflite/services/DbService.dart';
import 'package:flutter/material.dart';

class ProgramsView extends StatefulWidget {
  List<orderListModel> data;

  ProgramsView(this.data);

  @override
  _ProgramsViewState createState() => _ProgramsViewState();
}

class _ProgramsViewState extends State<ProgramsView> {
  bool onCheckFav(id) {
    // we made this to check favourit button on the begin
    var fav = false;
    widget.data.forEach((v) {
      if (v.xId == id) {
        fav = true;
      }
    });
    return fav;
  }

  var db = DbService.db;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Programs',
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(32.0),
        child: ListView.separated(
          itemCount: programList.length,
          separatorBuilder: (context, index) => Divider(),
          itemBuilder: (context, index) {
            return ListTile(
              title: Text('${programList[index].xName}'),
              trailing: GestureDetector(
                onTap: () {
                  if (onCheckFav(programList[index].xId)) {
                    programList[index].isFavourit = false;
                    db.deleteProgram(programList[index].xId);
                    db.loadPrograms().then((v) {
                      setState(() {
                        widget.data = v;
                      });
                    });
                  } else {
                    programList[index].isFavourit = true;
                    db.insertProgram(orderListModel(
                      xName: programList[index].xName,
                      xTime: programList[index].xTime,
                      xId: programList[index].xId,
                    ));
                    db.loadPrograms().then((v) {
                      setState(() {
                        widget.data = v;
                      });
                    });
                  }
                },
                child: Icon(
                  onCheckFav(programList[index].xId)
                      ? Icons.favorite
                      : Icons.favorite_border,
                  color: Colors.red,
                ),
              ),
              // selected: programList[index].selected,
            );
          },
        ),
      ),
    );
  }
}
