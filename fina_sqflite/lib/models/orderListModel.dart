class orderListModel {
  final String xName;
  final String xId;
  final String xTime;
  bool isFavourit = false;
  orderListModel({
    this.xName,
    this.xId,
    this.xTime,
  });

  factory orderListModel.fromJson(Map<String, dynamic> json) =>
      new orderListModel(
        xName: json["xName"],
        xId: json["xId"],
        xTime: json["xTime"],
      );

  Map<String, dynamic> toJson() => {
        "xName": xName,
        "xId": xId,
        "xTime": xTime,
      };
}

final programList = [
  orderListModel(
    xName: "OrderNumber1",
    xId: "1",
    xTime: "10:12"
  ),
  orderListModel(
      xName: "OrderNumber2",
      xId: "2",
      xTime: "12:10"
  ),
];
